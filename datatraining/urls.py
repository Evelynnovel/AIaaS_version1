from django.urls import path

from .import views

app_name = 'datatraining'
urlpatterns = [
    path('sent_data/', views.sent_data, name = 'sent_data'),
    path('choose_update_or_insert', views.choose_update_or_insert, name = 'choose_update_or_insert'),
    path('preprocess_keep', views.preprocess_keep, name = 'preprocess_keep'),
    path('preprocess', views.preprocess, name = 'preprocess'),
    path('train', views.train, name = 'train'),
]