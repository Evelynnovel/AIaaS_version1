from django.apps import AppConfig


class DatatrainingConfig(AppConfig):
    name = 'datatraining'
