from django.db import models
from django.contrib.auth.models import User

class CSV_File(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    originfilename = models.CharField(max_length = 255)
    user_defined_filename = models.CharField(max_length = 255)
    filename = models.CharField(max_length = 255)
    path = models.CharField(max_length = 255)
    shared = models.BooleanField()
    upload_at = models.DateTimeField(auto_now_add = True)

class Picture(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    originfilename = models.CharField(max_length = 255)
    user_defined_filename = models.CharField(max_length = 255)
    filename = models.CharField(max_length = 255)
    path = models.CharField(max_length = 255)
    shared = models.BooleanField()
    upload_at = models.DateTimeField(auto_now_add = True)

class TrainModel(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    originfilename = models.CharField(max_length = 255)
    user_defined_filename = models.CharField(max_length = 255)
    filename = models.CharField(max_length = 255)
    path = models.CharField(max_length = 255)
    description = models.CharField(max_length = 255)
    algorithm = models.CharField(max_length = 255)
    shared_API = models.CharField(max_length = 255)
    shared = models.BooleanField()
    upload_at = models.DateTimeField(auto_now_add = True)