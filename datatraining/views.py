from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.datastructures import MultiValueDictKeyError
from .models import TrainModel, CSV_File

import requests
import json
import urllib.request
import os


#35.229.224.102
global ip
ip = 'http://localhost:9999/'
global data_url 
data_url = os.path.abspath('.') + '/media/'

@login_required()
def sent_data(request):       
    if request.method == 'POST':
        try:
            data = request.FILES['myfile']
            global data_name
            data_name = data.name
            global data_url
            with open(data_url + data_name, 'wb') as destination:
                for chunk in data.chunks():
                    destination.write(chunk)
            user_id = request.user.id
            user = User.objects.get(id = user_id)
            global shared
            if request.POST.get('shared', False):
                shared = 1
            else:
                shared = 0
            global test_exixts
            try:
                test_exixts = CSV_File.objects.get(user_id = user_id, user_defined_filename = data.name)
            except CSV_File.DoesNotExist:
                test_exixts = None
        
            if test_exixts != None:
                return redirect('/datatraining/choose_update_or_insert')
        
            url = ip + 'upload'
            post_data = {'userid': user_id}
            files = {'file': data}        
            response = requests.post(url, files = files, data = post_data)

            res_json = response.json()
            status = res_json['status']
            message = res_json['message']      
        
            if status == 'success':
                path = res_json['data']
                originfilename = res_json['origin_filename']
                filename = res_json['filename']
            
                new_csv = CSV_File()
                new_csv.originfilename = originfilename
                new_csv.user_defined_filename = originfilename
                new_csv.filename = filename
                new_csv.path = path
                new_csv.shared = shared
                new_csv.user = user
                new_csv.save()
        
            return render(request, 'datatraining/sent_data.html',{'json': res_json})
        except MultiValueDictKeyError:
            msg = 'You did not choose any file'
            return render(request, 'datatraining/sent_data.html', {'msg': msg})
    else:
        return render(request, 'datatraining/sent_data.html')

@login_required()
def choose_update_or_insert(request):
    user_id = request.user.id
    global test_exixts
    global data_name
    global data_url
    data = open(data_url + data_name, 'rb')

    if request.method == 'POST':
        if request.POST['method'] == 'update':
            #delete old file
            url = ip + 'deldata'
            post_data = {"data_route": test_exixts.path}
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            response = requests.post(url, data = json.dumps(post_data), headers = headers)

            res_json = response.json()
            status = res_json['status']

            if status != 'success':
                msg = 'Fail to update'
                return render(request, 'datatraining/choose_update_or_insert.html', {'msg': msg})
            
            #update old file to new file
            url = ip + 'upload'
            post_data = {'userid': user_id}
            files = {'file': data}        
            response = requests.post(url, files = files, data = post_data)

            res_json = response.json()
            status = res_json['status']                
            if status == 'success':
                path = res_json['data']
                originfilename = res_json['origin_filename']
                filename = res_json['filename']

                test_exixts.path = path
                test_exixts.originfilename = originfilename
                test_exixts.user_defined_filename = originfilename
                test_exixts.filename = filename
                test_exixts.shared = shared
                test_exixts.upload_at = timezone.now()
                test_exixts.save()

                msg = 'Update successfully'
            else:
                msg = 'Fail to update'
            
            return render(request, 'datatraining/choose_update_or_insert.html', {'msg': msg})
        else:
            count = 0
            while(test_exixts != None):
                count += 1
                name = data_name
                name = name[:-4]
                name = name + '_' + str(count) + '.csv'
                try:
                    test_exixts = CSV_File.objects.get(user_id = user_id, user_defined_filename = name)
                except CSV_File.DoesNotExist:
                    test_exixts = None
            
            url = ip + 'upload'
            post_data = {'userid': user_id}
            files = {'file': data}        
            response = requests.post(url, files = files, data = post_data)

            res_json = response.json()
            status = res_json['status']
            msg = res_json['message']

            if status == 'success':
                path = res_json['data']
                originfilename = res_json['origin_filename']
                filename = res_json['filename']
                user = User.objects.get(id = user_id)

                new_csv = CSV_File()
                new_csv.originfilename = originfilename
                new_csv.user_defined_filename = name
                new_csv.filename = filename
                new_csv.path = path
                new_csv.shared = shared
                new_csv.user = user
                new_csv.save()
            return render(request, 'datatraining/choose_update_or_insert.html', {'msg': msg})
    else:
        return render(request, 'datatraining/choose_update_or_insert.html')

@login_required()
def preprocess_keep(request):
    keep = []
    url = ip + 'prep'
    response = urllib.request.urlopen(url)
    res_json = json.loads(response.read().decode())
    
    msg = res_json['message']
    data = res_json['data']
    plotdata = res_json['plotdata']
    
    if request.method == 'POST':

        global col
        col = []
        for d in data:
            col.append(d['col'])

        for value in request.POST.getlist('keep'):
            for c in col:
                if value == c:
                    keep.append(value)

        global passkeep
        passkeep = keep

        return render(request, 'datatraining/preprocess_keep.html', {'keep': passkeep})
    else:
        return render(request, 'datatraining/preprocess_keep.html',
                                {'msg': msg, 'data': data})

@login_required()
def preprocess(request):
    global ip
    url = ip + 'prep'
    response = urllib.request.urlopen(url)
    res_json = json.loads(response.read().decode())

    status = res_json['status']
    msg = res_json['message']
    data = res_json['data']

    keep = []
    cat = []
    na = ''

    global passkeep
    for i in data:
        for j in passkeep:
            if i['col'] == j:
                keep.append(i)

    if request.method == 'POST':     
        keep = str(passkeep)
        global col
        for value in col:
            if request.POST.get(value, False):
                v = request.POST.get(value, False)
                na += '"' + value + '":' + v +','
        na = na[:-1]

        for value in request.POST.getlist('cat'):
            if value != 'integer':
                cat.append(value)
        cat = str(cat)

        post_data = '{"keep":' + keep + ',"na":{' + na + '},"cat":' + cat + '}'
        url = ip + 'confirm'
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        response = requests.post(url, data = json.dumps(eval(post_data)), headers = headers)

        res_json = response.json()
        res_status = res_json['status']
        res_msg = res_json['message']
        res_data = res_json['data']

        return render(request, 'datatraining/preprocess.html',
                        {'res_status': res_status, 'res_msg': res_msg})
    else:
        return render(request, 'datatraining/preprocess.html',
                        {'msg': msg, 'keep': keep})

@login_required()
def train(request):
    global passkeep
    user_id = request.user.id
    algorithm = [{"algorithm": "RandomForest", "index": 0},
                 {"algorithm": "LogisticRegression", "index": 1},
                 {"algorithm": "SVM", "index": 2},
                 {"algorithm": "Kernel SVM", "index": 3},
                 {"algorithm": "KNN", "index": 4},
                 {"algorithm": "Xgboost", "index": 5},]

    if request.method == 'POST':
        if request.POST.get('trainallkeep', False):
            algo_index = request.POST['trainallkeep']
            post_data = '{"algo":' + algo_index + '}'
            url = ip + 'trainallkeep'
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            response = requests.post(url, data = json.dumps(eval(post_data)), headers = headers)

            res_json = response.json()
            status = res_json['status']
            msg = res_json['message']
            if request.POST.get('shared', False):
                shared = 1
            else:
                shared = 0
            user = User.objects.get(id = user_id)
            
            if status == 'success':
                originfilename = res_json['origin_filename']
                filename = res_json['filename']
                path = res_json['model_route']

                new_model = TrainModel()
                new_model.originfilename = originfilename
                new_model.user_defined_filename = originfilename
                new_model.filename = filename
                new_model.path = path
                new_model.algorithm = algo_index
                new_model.shared = shared
                new_model.user = user
                new_model.save()


            return render(request, 'datatraining/train.html', {'msg': msg})
        else:
            algo_index = request.POST['algo']
            target = request.POST['target']
            test_pro = request.POST['test_pro']

        if algo_index == '6':
            post_data = '{"target":"' + target + '", "test_pro":' + test_pro + '}'
            url = ip + 'trainall'
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            response = requests.post(url, data = json.dumps(eval(post_data)), headers = headers)

            res_json = response.json()          
            return render(request, 'datatraining/train.html', 
                        {'res_json': res_json, 'algorithm': algorithm})
        else:
            post_data = '{"algo":' + algo_index + ',"target":"' + target + '","test_pro":' + test_pro + '}'
            url = ip + 'train'
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            response = requests.post(url, data = json.dumps(eval(post_data)), headers = headers)

            res_json = response.json()
            accuracy = res_json['accuracy']
            
            algo = ''
            for element in algorithm:
                if str(element['index']) == algo_index:
                    algo += element['algorithm']
            originfilename = res_json['origin_filename']
            filename = res_json['filename']
            path = res_json['model_route']
            if request.POST.get('shared', False):
                shared = 1
            else:
                shared = 0
            user = User.objects.get(id = user_id)

            new_model = TrainModel()
            new_model.originfilename = originfilename
            new_model.user_defined_filename = originfilename
            new_model.filename = filename
            new_model.path = path
            new_model.algorithm = algo_index
            new_model.shared = shared
            new_model.user = user
            new_model.save()
            
            return render(request, 'datatraining/train.html', {'accuracy': accuracy, 'algo': algo})
    else:
        return render(request, 'datatraining/train.html', {'keep': passkeep, 'algorithm': algorithm})