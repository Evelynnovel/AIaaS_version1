from django.shortcuts import render, redirect
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse, StreamingHttpResponse
from datatraining.models import TrainModel, CSV_File

import json
import requests
import string
import os

#35.229.224.102
global ip
ip = 'http://localhost:9999/'

def login(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            return redirect('/')
    
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')

        user = auth.authenticate(username = username, password = password)
        if user is not None and user.is_active:
            auth.login(request, user)
            return redirect('/')
        else:
            msg = 'Fail to Login!'
            return render(request, 'account/login.html', {'msg': msg})
    else:
        return render(request, 'account/login.html')

@login_required()
def logout(request):
    auth.logout(request)
    return redirect('/')

def register(request):
    username = ''

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        password_confirm = request.POST['password_confirm']

        test_username = User.objects.filter(username = username)
        if test_username.exists():
            msg = 'This username has already been used by someone.'
            return render(request, 'account/register.html', {'msg': msg})

        if len(password) < 8:
            msg = 'Your password must contains at least 8 characters.'
            return render(request, 'account/register.html', {'msg': msg, 'username': username})

        if password.isdigit():
            msg = 'Your password are entirely numeric.'
            return render(request, 'account/register.html', {'msg': msg, 'username': username})

        if password != password_confirm:
            msg = 'The password you entered does not match.'
            return render(request, 'account/register.html', {'msg': msg, 'username': username})

        new_user = User.objects.create_user(username = username, password = password)

        user = auth.authenticate(username = username, password = password)
        auth.login(request, user)
        return redirect('/')
    else:
        return render(request, 'account/register.html')

@login_required()
def user_profile(request):
    user_id = request.user.id
    return render(request, 'account/user_profile.html')

@login_required()
def account_setting(request):
    user_id = request.user.id
    user = User.objects.get(id = user_id)

    if request.method == 'POST':
        username = request.POST['username']
        first_name = request.POST.get('first_name', '')
        last_name = request.POST.get('last_name', '')
        email = request.POST['email']

        test_username = User.objects.filter(username = username)
        test_username = test_username.exclude(id = user_id)
        if test_username.exists():
            msg = 'This username has already been used by someone.'
            return render(request, 'account/account_setting.html',
                {'msg': msg, 'first_name': first_name, 'last_name': last_name, 'email': email})
        
        test_email = User.objects.filter(email = email)
        test_email = test_email.exclude(id = user_id)
        if test_email.exists():
            msg = 'This e-mail has already been used by someone.'
            return render(request, 'account/account_setting.html',
                {'msg': msg, 'first_name': first_name, 'last_name': last_name, 'username': username})
        
        user.username = username
        user.first_name = first_name
        user.last_name = last_name
        user.email = email
        user.save()
        msg = 'Your setting has been saved.'

        return render(request, 'account/account_setting.html',
            {'msg': msg, 'first_name': first_name, 'last_name': last_name, 'username': username,
            'email': email})
    else:
        username = user.username
        first_name = user.first_name
        last_name = user.last_name
        email = user.email

        return render(request, 'account/account_setting.html',
            {'username': username, 'first_name': first_name, 'last_name': last_name, 'email': email})

@login_required()
def model_manage(request):
    user_id = request.user.id
    model_list = TrainModel.objects.filter(user_id = user_id)
    model = []
    for element in model_list:
        model.append(element.user_defined_filename)

    if request.method == 'POST':
        if request.POST.get('download', False):
            download_filename = request.POST['download']
            download_model = TrainModel.objects.get(user_id = user_id, user_defined_filename = download_filename)
            download_path = download_model.path

            post_data = {'model_route': download_path}
            url = ip + 'getmodel'
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            file = requests.post(url, data = json.dumps(post_data), headers = headers)

            response = StreamingHttpResponse(file)
            response['Content-Type'] = 'application/octet-stream'
            response['Content-Disposition'] = 'attachment;filename = "{0}"'.format(download_filename)

            return response
        elif request.POST.get('delete', False):
            global req_delete_model
            req_delete_model = request.POST['delete']
            return redirect('/account/model_delete')
        elif request.POST.get('edit', False):
            global req_edit_model
            req_edit_model = request.POST['edit']
            return redirect('/account/model_edit')
        elif request.POST.get('search_model', False):
            search_model = request.POST['search_model']
            model_list = TrainModel.objects.filter(user_id = user_id, user_defined_filename__contains = search_model)
            if model_list.exists():
                model = []
                for element in model_list:
                    model.append(element.user_defined_filename)
                return render(request, 'account/model_manage.html', {'model': model})
            else:
                msg = 'There are no model having this filename.'
                return render(request, 'account/model_manage.html', {'model': model, 'msg': msg})
    else:
        return render(request, 'account/model_manage.html', {'model': model})

@login_required()
def model_delete(request):
    user_id = request.user.id
    global req_delete_model

    if request.method == 'POST':
        if request.POST['delete'] == '1':
            delete_model = TrainModel.objects.get(user_id = user_id, user_defined_filename = req_delete_model)
            delete_path = delete_model.path

            url = ip + 'delmodel'
            post_data = {"model_route": delete_path}
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            response = requests.post(url, data = json.dumps(post_data), headers = headers)

            res_json = response.json()
            status = res_json['status']

            if status == 'success':
                delete_model.delete()
                delete_model = TrainModel.objects.filter(user_id = user_id, user_defined_filename = req_delete_model)
                if delete_model.count() != 0:
                    msg = 'Fail to delete'
                else:
                    msg = 'Delete successfully'
            else:
                msg = 'Fail to delete'

            return render(request, 'account/model_delete.html', {'msg': msg})
        else:
            return redirect('/account/model_manage')
    else:
        return render(request, 'account/model_delete.html', {'delete_model': req_delete_model})

@login_required()
def model_edit(request):
    user_id = request.user.id
    global req_edit_model

    if request.method == 'POST':
        new_modelname = request.POST['new_modelname']
        if request.POST.get('shared', False):
            shared = 1
        else:
            shared = 0       
        edit_model = TrainModel.objects.get(user_id = user_id, user_defined_filename = req_edit_model)
        
        test_exists = TrainModel.objects.filter(user_id = user_id, user_defined_filename = new_modelname)
        if test_exists.count != 0:
            msg = 'You have already the file with the same name. Please choose another filename.'
        else:
            edit_model.user_defined_filename = new_modelname
            edit_model.shared = shared
            edit_model.save()

            edit_model = TrainModel.objects.filter(user_id = user_id, user_defined_filename = new_modelname)
            if edit_model.count() != 0:
                msg = 'success'
            else:
                msg = 'fail'
        
        return render(request, 'account/model_edit.html', {'msg': msg})
    else:
        return render(request, 'account/model_edit.html',{'model': req_edit_data})

@login_required()
def data_manage(request):
    user_id = request.user.id
    csv_list = CSV_File.objects.filter(user_id = user_id)
    csv_file = []  
    for element in csv_list:
        csv_file.append(element.user_defined_filename)

    if request.method == 'POST':       
        if request.POST.get('download', False):
            download_filename = request.POST['download']
            download_csv = CSV_File.objects.get(user_id = user_id, user_defined_filename = download_filename)
            download_path = download_csv.path

            post_data = {'data_route': download_path}
            url = ip + 'getdata'
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            file = requests.post(url, data = json.dumps(post_data), headers = headers)

            response = StreamingHttpResponse(file)
            response['Content-Type'] = 'application/octet-stream'
            response['Content-Disposition'] = 'attachment;filename = "{0}"'.format(download_filename)

            return response     
        elif request.POST.get('delete', False):
            global req_delete_data
            req_delete_data = request.POST['delete']
            return redirect('/account/data_delete')
        elif request.POST.get('edit', False):
            global req_edit_data
            req_edit_data = request.POST['edit']
            return redirect('/account/data_edit')
        elif request.POST.get('search_data', False):
            search_data = request.POST['search_data']
            csv_list = CSV_File.objects.filter(user_id = user_id, user_defined_filename__contains = search_data)
            if csv_list.exists():
                csv_file = []  
                for element in csv_list:
                    csv_file.append(element.user_defined_filename)         
                return render(request, 'account/data_manage.html', {'csv_file': csv_file})
            else:
                msg = 'There are no data having this filename.'
                return render(request, 'account/data_manage.html', {'csv_file': csv_file, 'msg': msg})
    else:
        return render(request, 'account/data_manage.html', {'csv_file': csv_file})

@login_required()
def data_delete(request):
    user_id = request.user.id
    global req_delete_data

    if request.method == 'POST':
        if request.POST['delete'] == '1':
            delete_csv = CSV_File.objects.get(user_id = user_id, user_defined_filename = req_delete_data)
            delete_path = delete_csv.path

            url = ip + 'deldata'
            post_data = {"data_route": delete_path}
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            response = requests.post(url, data = json.dumps(post_data), headers = headers)

            res_json = response.json()
            status = res_json['status']

            if status == 'success':
                delete_csv.delete()
                delete_csv = CSV_File.objects.filter(user_id = user_id, user_defined_filename = req_delete_data)      
                if delete_csv.count() != 0:
                    msg = 'Fail to delete'
                else:
                    msg = 'Delete successfully'
            else:
                msg = 'Fail to delete'
            
            return render(request,'account/data_delete.html', {'msg': msg})
        else:
            return redirect('/account/data_manage')
    else:
        return render(request, 'account/data_delete.html', {'delete_file': req_delete_data})

@login_required()
def data_edit(request):
    user_id = request.user.id
    global req_edit_data
    
    if request.method == 'POST':
        new_filename = request.POST['new_filename']
        if request.POST.get('shared', False):
            shared = 1
        else:
            shared = 0

        csv = CSV_File.objects.get(user_id = user_id, user_defined_filename = req_edit_data)

        test_exists = CSV_File.objects.filter(user_id = user_id, user_defined_filename = new_filename)
        if test_exists.count() != 0:
            msg = 'You have already the file with the same name. Please choose another filename.'
        else:
            csv.user_defined_filename = new_filename
            csv.shared = shared
            csv.save()

            csv = CSV_File.objects.filter(user_id = user_id, user_defined_filename = new_filename)
            if csv.count() != 0:
                msg = 'Edit successfully'
            else:
                msg = 'Fail to edit'

        return render(request, 'account/data_edit.html', {'msg': msg})
    else:
        return render(request, 'account/data_edit.html',{'data': req_edit_data})