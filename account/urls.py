from django.urls import path, include

from .import views

app_name = 'account'
urlpatterns = [
    path('login/', views.login, name = 'login'),
    path('logout/', views.logout, name = 'logout'),
    path('register/', views.register, name = 'register'),
    
    path('user_profile/', views.user_profile, name = 'user_profile'),
    path('account_setting', views.account_setting, name = 'account_setting'),

    path('data_manage', views.data_manage, name = 'data_manage'),
    path('model_manage', views.model_manage, name = 'model_manage'),
    path('data_edit', views.data_edit, name = 'data_edit'),
    path('model_edit', views.model_edit, name = 'model_edit'),
    path('data_delete', views.data_delete, name = 'data_delete'),
    path('model_delete', views.model_delete, name = 'model_delete'),
]
