from django.contrib import admin
from django.contrib.auth import views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include('homepage.urls')),
    path('account/', include('account.urls')),
    path('datatraining/', include('datatraining.urls')),
    path('auth/', include('social_django.urls', namespace = 'social')),
    path('login/', views.login, name = 'login'),
    path('logout/', views.logout, name = 'logout'),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
