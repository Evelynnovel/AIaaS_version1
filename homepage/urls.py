from django.urls import path

from .import views

app_name = 'homepage'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('public_data', views.shared_data, name = 'shared_data'),
    path('public_model', views.shared_model, name = 'shared_model'),
]
