from django.shortcuts import render
from django.http import HttpResponse, StreamingHttpResponse
from django.contrib.auth.models import User
from datatraining.models import TrainModel, CSV_File

import requests
import json

#35.229.224.102
global ip
ip = 'http://localhost:9999/'

def index(request):
    return render(request, 'homepage/index.html')

def shared_data(request):
    csv_list = CSV_File.objects.filter(shared = 1)  
    
    if csv_list.exists():
        csv = '['
        for element in csv_list:
            csv += '{"filename":"' + element.user_defined_filename + '", "username":"'
            user = User.objects.get(id = element.user_id)
            csv += user.username + '", "id":' + str(element.id) + '},'         
        csv = csv[:-1]
        csv += ']'
        csv = eval(csv)

        if request.POST.get('download', False):
            download_file_id = request.POST['download']
            download_csv = CSV_File.objects.get(id = download_file_id)
            download_path = download_csv.path
            download_filename = download_csv.user_defined_filename

            post_data = {'data_route': download_path}
            url = ip + 'getdata'
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            file = requests.post(url, data = json.dumps(post_data), headers = headers)

            response = StreamingHttpResponse(file)
            response['Content-Type'] = 'application/octet-stream'
            response['Content-Disposition'] = 'attachment;filename = "{0}"'.format(download_filename)

            return response
        elif request.POST.get('search_data', False):
            search_data = request.POST['search_data']
            csv_list = CSV_File.objects.filter(shared = 1, user_defined_filename__contains = search_data)
            if csv_list.exists():
                csv = '['
                for element in csv_list:
                    csv += '{"filename":"' + element.user_defined_filename + '", "username":"'
                    user = User.objects.get(id = element.user_id)
                    csv += user.username + '", "id":' + str(element.id) + '},'         
                csv = csv[:-1]
                csv += ']'
                csv = eval(csv)
                return render(request, 'homepage/shared_data.html', {'shared_csv': csv})
            else:
                msg = 'There are no sharing data having this filename.'
                return render(request, 'homepage/shared_data.html', {'shared_csv': csv, 'msg': msg})
        else:
            return render(request, 'homepage/shared_data.html', {'shared_csv': csv})
    else:
        return render(request, 'homepage/shared_data.html')

    

def shared_model(request):
    model_list = TrainModel.objects.filter(shared = 1)

    if model_list.exists():
        model = '['
        for element in model_list:
            model += '{"filename":"' + element.user_defined_filename + '", "username":"'
            user = User.objects.get(id = element.user_id)
            model += user.username + '", "id": ' + str(element.id) + '},'
        model = model[:-1]
        model += ']'
        model = eval(model)

        if request.POST.get('download', False):
            download_file_id = request.POST['download']
            download_model = TrainModel.objects.get(id = download_file_id)
            download_path = download_model.path
            download_filename = download_model.user_defined_filename

            post_data = {'model_route': download_path}
            url = ip + 'getmodel'
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            file = requests.post(url, data = json.dumps(post_data), headers = headers)

            response = StreamingHttpResponse(file)
            response['Content-Type'] = 'application/octet-stream'
            response['Content-Disposition'] = 'attachment;filename = "{0}"'.format(download_filename)

            return response
        elif request.POST.get('search_model', False):
            search_model = request.POST['search_model']
            model_list = TrainModel.objects.filter(shared = 1, user_defined_filename__contains = search_model)
            if model_list.exists():
                model = '['
                for element in model_list:
                    model += '{"filename":"' + element.user_defined_filename + '", "username":"'
                    user = User.objects.get(id = element.user_id)
                    model += user.username + '", "id": ' + str(element.id) + '},'
                model = model[:-1]
                model += ']'
                model = eval(model)
                return render(request, 'homepage/shared_model.html', {'shared_model': model})
            else:
                msg = 'There are no sharing model having this filename.'
                return render(request, 'homepage/shared_model.html', {'shared_model': model, 'msg': msg})
        else:
            return render(request, 'homepage/shared_model.html', {'shared_model': model})
    else:
        msg = 'There are no sharing model.'
        return render(request, 'homepage/shared_model.html') 